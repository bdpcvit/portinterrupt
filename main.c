/*******************************************************************************
*
* PortInterrupt: Test project to look on to Ports interrupt 
*  process for MSP430 MCU
*
*    PortInterrupt:              copyright � 2015, Dmytro Bernyk.
*
*    THIS SOFTWARE IS PROVIDED BY THE DMITRY BERNYK AND CONTRIBUTORS "AS IS"
*    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
*    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DMITRY FRANK OR CONTRIBUTORS BE
*    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*    THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************/

#include "io430.h"
#include <stdint.h>
#include <intrinsics.h>

void main(void)
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;
  
  BCSCTL3 = LFXT1S1;    //VLOCLK use as sourse for ACLOCK
  DCOCTL = 0x00;
  DCOCTL = CALDCO_1MHZ; //User 1MGHz calibrate value to config DCO
  BCSCTL1 = CALBC1_1MHZ;
  
  P1DIR = 0x00;     //All pins in port P1 configure as Input
  
  
  
  while(1)
}
